# Security Triage Automation changelog

## v1.0.3

- release 1.0.3

## v1.0.2

- release 1.0.2

## v1.0.1

- Updated release

## v1.0.0

- Initial release
